const getUrl = (e) => {
    return e.request.url;
};

const CACHE_NAME = 'team-2';

const config = {
    offlineUrls: [
        '/',
        '/static/index.js',
        '/static/style.css',
        '/static/offline.js',
        '/static/offline.css',
    ],
    cachePathPattern: /(.+)/,
};

const DEBUG_MODE = null;

const log = (msg, force) => {
    if (DEBUG_MODE || force) {
        console.log.apply(console, [msg]);
    }
};

/**
 * Register files for offline
 * @return {Promise}
 */
const installOfflineScripts = () => {
    return 	caches.open(CACHE_NAME)
        .then((cache) => {
            log(`ACTIVATE:install  ${config.offlineUrls}`);
            return cache.addAll(config.offlineUrls);
        })
        .catch((err) => {
            log(`ACTIVATE:install:error  ${err}`);
        });
};

/**
 * Check is Cache Key is actual
 * @return {Promise}
 */
const ensureCacheStorageKey = () => {
    return caches.keys().then((keyList) => {
        log(`ACTIVATE::keyList ${keyList}`);
        return Promise.all(keyList.map((key) => {
            log(`ACTIVATE::key ${key}`);
            if (CACHE_NAME !== key) {
                log(`ACTIVATE::will-drop ${key}`);
                return caches.delete(key);
            }

            return Promise.resolve();
        }));
    });
};

const shouldHandleFetch = (event) => {
    const request = event.request;
    const url = new URL(request.url);
    const criteria = {
        matchesPathPattern: !!(config.cachePathPattern.exec(url.pathname)),
        isGETRequest: request.method === 'GET',
        isFromMyOrigin: url.origin === self.location.origin,
    };

    const failingCriteria = Object.keys(criteria).filter((criteriaKey) => {
        return !criteria[criteriaKey];
    });

    log(criteria);
    log(url.pathname);
    return !failingCriteria.length;
};

const addToCache = (request, response) => {
    log(`fetch:try add to cache - ${getUrl({request: request})}`);
    if (response.ok) {
        const copy = response.clone();
        caches.open(CACHE_NAME).then((cache) => {
            log(`fetch:addToCache - ${getUrl({request: request})}`);
            cache.put(request, copy);
        });
    }

    return response;
};

const fetchFromCache = (event) => {
    return caches.match(event.request).then((response) => {
        log(`try from cache - ${getUrl(event)}`, 1);
        if (!response) {
            throw Error(`${event.request.url} not found in cache`);
        }

        return response;
    });
};

const onFetch = (event) => {
    const request = event.request;
    const acceptHeader = request.headers.get('Accept');

    const resourceType = ~acceptHeader.indexOf('text/html') ? 'content' : 'static';

    log(`onFetch: ${getUrl(event)}`, 1);
    log(`acceptHeader: `, 1);
    log(acceptHeader, 1);
    log(request, 1);
    log(resourceType, 1);

    if (resourceType === 'content') {
        log('fetch:content', 1);
        event.respondWith(
            fetch(request)
                .then((response) => {
                    log(`fetch:internet - ${getUrl({request: request})}`, 1);
                    return addToCache(request, response);
                })
                .catch(() => {
                    log(`fetch:cache - ${getUrl({request: request})}`, 1);
                    return fetchFromCache(event);
                })
        );
    } else {
        log('fetch:not content');
        event.respondWith(
            fetchFromCache(event)
                .catch(() => fetch(request))
                .then(response => addToCache(request, response))
        );
    }
};

log('----- AAA -----');
self.addEventListener('install', (event) => {
    log('----- INSTALL -----');
    event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', (event) => {
    log('----- ACTIVATE -----');

    event.waitUntil(Promise.all([
        installOfflineScripts(),
        ensureCacheStorageKey(),
    ]));
});

self.addEventListener('fetch', (event) => {
    log('----- FETCH -----');
    log(`start fetch - ${getUrl(event)}`);
    if (shouldHandleFetch(event)) {
        onFetch(event);
    } else {
        log(`even not try - ${getUrl(event)}`);
    }
});
