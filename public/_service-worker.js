const getUrl = (e) => {
    return e.request.url;
};

const CACHE_NAME = 'team-v2';

const offlineUrls = [
    '/',
    '/static/index.js',
    '/static/style.css',
    '/static/offline.js',
    '/static/offline.css',
];

const installOfflineScripts = () => {
    return 	caches.open(CACHE_NAME)
        .then((cache) => {
            console.log(`ACTIVATE:install  ${offlineUrls}`);
            return cache.addAll(offlineUrls);
        })
        .catch((err) => {
            console.log(`ACTIVATE:install:error  ${err}`);
        });
};

const enshureCacheKey = () => {
    const cacheWhitelist = [CACHE_NAME];
    return caches.keys().then((keyList) => {
        console.log(`ACTIVATE::keyList ${keyList}`);
        return Promise.all(keyList.map((key) => {
            console.log(`ACTIVATE::key ${key}`);
            if (!~cacheWhitelist.indexOf(key)) {
                console.log(`ACTIVATE::will-drop ${key}`);
                return caches.delete(key);
            }

            return Promise.resolve();
        }));
    });
};

self.addEventListener('install', (event) => {
    console.log('----- INSTALL -----');
    event.waitUntil(self.skipWaiting());
});

self.addEventListener('activate', (event) => {
    console.log('----- ACTIVATE -----');

    event.waitUntil(Promise.all([installOfflineScripts(), enshureCacheKey()]));
});

self.addEventListener('fetch', (event) => {
    console.log('----- FETCH -----');
    console.log(`fetch: ${getUrl(event)}`);
    event.respondWith(
        caches.match(event.request)
            .then((res) => {
                if (res) {
                    console.log(`fetch:caches.match::ok ${getUrl(event)}`);
                    return res;
                }

                console.log(`fetch:caches.match::404 ${getUrl(event)}`);
                return fetch(event.request);
            })
            .catch((err) => {
                console.log(`fetch:caches.match::catch ${getUrl(event)}`);
                return fetch(event.request);
            })
    );
});