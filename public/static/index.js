let index = 0;
console.log(++index + 1);
document.querySelector('#root').innerText = index;

// Check for browser support of service worker
if ('serviceWorker' in navigator) {
	navigator.serviceWorker.register('/service-worker.js')
		.then((reg) => {
			console.log('Hooray. Registration successful, scope is:', reg.scope);
		}).catch((err) => {
			console.log('Whoops. Service worker registration failed, error:', err);
		});
}